import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Card, CardContent } from "@material-ui/core/";
import emptyRepoIcon from "../../assets/images/empty-repo-avatar.svg";

const useStyles = makeStyles({
  root: {
    minWidth: 180,
    padding: 0,
    margin: 0,

    "& .MuiCardContent-root:last-child": {
      padding: 0,
      minHeight: "8em",
      borderRadius: "0",
    },
  },
  mainContainer: { width: "calc(100% - 20%)", margin: "0 auto" },
  marginTop: {
    marginTop: "2em",
  },
  header: {
    height: "3em",
    width: "100%",
    backgroundColor: "#F4F5F7",
  },
  setImage: {
    position: "relative",
    top: "2em",
    left: "1em",
  },
  body: {
    padding: "1em",
    textAlign: "center",
    position: "relative",
    top: "1.2em",
  },
  sections: {
    marginTop: "1em",
    marginBottom: "1em",
  },
  pullSection: {
    marginTop: "2em",
    marginBottom: "1em",
  },
  divContainer: {
    marginTop: "1em",
    minHeight: "4em",
    textAlign: "center",
    borderTop: "2px solid #DFE1E6",
    borderBottom: "2px solid #DFE1E6",
    padding: "3em",
  },
});

const YourWork = () => {
  const classes = useStyles();
  return (
    <>
      <div className={classes.mainContainer}>
        <section className={classes.sections}>
          <h5>Recent repositories</h5>
          <Grid container className={classes.marginTop}>
            <Grid item xs={2}>
              <Card className={classes.root}>
                <CardContent>
                  <div>
                    <div className={classes.header}>
                      <img
                        className={classes.setImage}
                        src={emptyRepoIcon}
                        alt="empty repo"
                      />
                    </div>
                    <div className={classes.body}>
                      <a href="/">Create repository</a>
                    </div>
                  </div>
                </CardContent>
              </Card>
            </Grid>
          </Grid>
        </section>
        <section className={classes.pullSection}>
          <h5>Pull requests</h5>
          <div className={classes.divContainer}>
            <span>You have no open pull requests.</span>
          </div>
        </section>
        <section className={classes.pullSection}>
          <h5>Jira Software issues</h5>
          <div className={classes.divContainer}>
            <span>
              Connect your commits, branches and pull requests to issues, and
              view, edit and transition issues from Bitbucket.
            </span>
            <br />
            <br />
            <a href="/">Connect Jira sites</a>
          </div>
        </section>
      </div>
    </>
  );
};

export default YourWork;
