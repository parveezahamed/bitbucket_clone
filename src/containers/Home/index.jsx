import React from "react";
import { Outlet } from "react-router-dom";
import StickyLeftNav from "../../components/StickyLeftNav";
import LeftNav from "../../components/LeftNav";

const Home = () => {
  return (
    <div className="bit_container">
      <StickyLeftNav />
      <LeftNav />
      <div className="main_container">
        <Outlet />
      </div>
    </div>
  );
};
export default Home;
