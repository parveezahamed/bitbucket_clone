import React, { useState, useEffect } from "react";
import { Grid } from "@material-ui/core/";
import { makeStyles } from "@material-ui/core/styles";
import Table from "../../components/Table/RepoTable";
import TableFilter from "../../components/Table/TableFilter";
import repos from "../../constents/repoConts";
import Button from "../../atoms/Button";
import Pagination from "../../atoms/Pagination";

const useStyles = makeStyles(() => ({
  headerContainer: {
    padding: "0 25px 0 25px",
    marginBottom: "20px",
  },
  header: {
    fontSize: "24px",
    fontWeight: 500,
  },
  textRight: {
    textAlign: "right",
  },
  setTopBorder: {
    width: "calc(100% - 5%)",
    margin: "0 auto",
    borderTop: "2px solid #DFE1E6",
    marginTop: "1em",
    marginBottom: "5em",
  },
}));

const Repositories = () => {
  const classes = useStyles();

  const [tableData, setTableData] = useState(repos);
  const [isData, showData] = useState(false);
  const [pageOfItems, setPageOfItems] = useState([]);

  useEffect(() => {
    if (repos.length > 0) {
      showData(true);
    }
  }, []);

  const searchedData = (filtered) => {
    filtered && filtered.length === 0 ? showData(false) : showData(true);

    setTableData(filtered);
    setPageOfItems(filtered);
  };

  const onChangePage = (pageOfItems) => {
    setPageOfItems(pageOfItems);
  };

  return (
    <div>
      <Grid container className={classes.headerContainer}>
        <Grid item xs={6}>
          <header className={classes.header}>Repositories</header>
        </Grid>
        <Grid item xs={6} className={classes.textRight}>
          <Button name="Create repository" />
        </Grid>
      </Grid>
      <TableFilter data={repos} searchedData={searchedData} />
      <Table data={pageOfItems} isData={isData} />
      <div className={classes.setTopBorder}>
        <Pagination items={tableData} onChangePage={onChangePage} />
      </div>
    </div>
  );
};

export default Repositories;
