export const unit = {
  spacing: 4,
};

export const colorCodes = {
  red: "#9d1d28",
  white: "#ffffff",
  darkGray: "#58595b",
  paleGray: "#f7f7f9",
  gray: "#C7C8CA",
  disabledGray: "#6b6b6b",
  lightGray: "#DCDDDE",
  whiteGray: "#FAFAFA",
  darkenGray: "#9C9EA0",
  tangerineYellow: "#ffcb05",
  lightningYellow: "#f5f6f8",
  darkBrown: "#BD7061",
  lightBrown: "#E8D1C9",
  rose: "#d8ada1",
  lightPurple: "#7f4d8d",
  lowOpacityPurple: "rgba(230, 202, 241, 0.27)",
  toledoBlack: "#272829",
  contentBgColor: "#FCFCFC",
  primary: "#9D1D27",
  success: "#2CA100",
  error: "#D0021B",
  transparent: "rgba(0,0,0,0)",
  grayBorder: "#bcbec0",
  borderColor: "#e2e3e3",
  mainBackground: "#fefefe",
  verticalBarColor: "#d9d9d9",
  green: "#529d1d",
  hoverBackground: "#f6f6fa",
  higlightHeader: "#4a4a4a",
  buttonColor: "#42526e",
};

export const fontSizes = {
  h1: {
    fontSize: "18px",
    fontWeight: "bold",
    lineHeight: "24px",
  },
  h2: {
    fontSize: "14px",
    fontWeight: "bold",
    lineHeight: "18px",
  },
  bodyText: {
    fontSize: "14px",
    fontWeight: "regular",
    lineHeight: "18px",
  },
  messageText: {
    fontSize: "14px",
    fontWeight: "obilique",
    lineHeight: "18px",
  },
  label: {
    fontSize: "11px",
    fontWeight: "regular",
    lineHeight: "14px",
    color: "#bcbec0",
  },
};
