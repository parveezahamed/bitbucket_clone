import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import React from "react";
import IconArrowDown from "../../assets/images/icon_arrow_down.svg";
import * as defaultTheme from "../default-theme";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: 0,
    minWidth: 120,
    width: "100%",
    "& .MuiSelect-select:focus": {
      backgroundColor: `${defaultTheme.colorCodes.transparent}`,
    },
    "& .MuiInputBase-input": {
      color: `${defaultTheme.colorCodes.darkGray}`,
      fontWeight: "bold",
      fontSize: `${defaultTheme.unit.spacing * 3.5}px`,
    },
    "& .MuiInputBase-root": {
      cursor: "pointer",
    },
    "& .MuiInput-underline:hover:not(.Mui-disabled):before ": {
      borderBottom: `1px solid ${defaultTheme.colorCodes.lightningYellow}`,
    },
    "& .MuiInput-underline:before ": {
      borderBottom: `1px solid ${defaultTheme.colorCodes.gray}`,
    },
    "& .MuiInput-underline:after": {
      borderBottom: `1px solid ${defaultTheme.colorCodes.lightningYellow}`,
    },
    "& .MuiFormLabel-root.Mui-focused ": {
      color: `${defaultTheme.colorCodes.darkenGray}`,
    },
    "& .MuiInputLabel-shrink": {
      fontSize: `${defaultTheme.unit.spacing * 4}px !important`,
      color: `${defaultTheme.colorCodes.darkenGray}`,
    },
    "& .MuiSelect-selectMenu": {
      fontSize: `${defaultTheme.unit.spacing * 3.5}px`,
      letterSpacing: "-0.16px",
      fontWeight: "bold",
      color: `${defaultTheme.colorCodes.darkGray}`,
    },
  },
  imgArrowIcon: {
    height: "9px",
    width: "16px",
  },
  dropdownStyle: {
    border: `solid ${defaultTheme.unit.spacing * 0.25}px ${
      defaultTheme.colorCodes.grayBorder
    } `,
    backgroundColor: `${defaultTheme.colorCodes.white}`,
    fontFamily: "Helvetica",
    fontSize: `${defaultTheme.unit.spacing * 3}px`,
  },
  disabledComponent: {
    opacity: 0.6,
  },
  labelStyle: {
    fontSize: `${defaultTheme.unit.spacing * 3.5}px`,
    lineHeight: "17px",
    letterSpacing: "-0.16px",
    color: `${defaultTheme.colorCodes.darkGray}`,
  },
  optionStyle: {
    fontSize: `${defaultTheme.unit.spacing * 3.5}px`,
    color: `${defaultTheme.colorCodes.darkGray}`,
  },
  setSpacing: {
    marginBottom: "1em",
  },
}));

export default function DropDown(props) {
  const classes = useStyles();
  const {
    value,
    name,
    handleChange,
    disabled,
    style,
    className,
    type,
    required,
    ...rest
  } = props;
  const dropDownIcon = () => {
    return <img className={classes.imgArrowIcon} src={IconArrowDown} alt="" />;
  };
  return (
    <div
      id="parent"
      className={`${classes.setSpacing} ${
        disabled ? classes.disabledComponent : ""
      }`}
      style={style}
    >
      <FormControl className={classes.formControl} required={required}>
        <InputLabel classes={{ root: classes.labelStyle }} id="select-label">
          {props.label}
        </InputLabel>
        <Select
          name={name}
          value={value}
          disabled={disabled}
          onChange={handleChange}
          IconComponent={(props) => <i {...props}>{dropDownIcon()}</i>}
          classes={{ disabled: disabled ? classes.underlineDisable : "" }}
          MenuProps={{
            classes: { paper: classes.dropdownStyle },
            getContentAnchorEl: null,
            anchorOrigin: {
              vertical: "bottom",
              horizontal: "left",
            },
          }}
          control={props.control}
          {...rest}
        >
          {props.items.map((item, i) => {
            return (
              <MenuItem
                key={i}
                value={type === "with" ? item.value : item.val}
                className={classes.optionStyle}
              >
                {type === "with" ? (
                  item.type
                ) : (
                  <span className={className}>{item.label}</span>
                )}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>
    </div>
  );
}

DropDown.propTypes = {
  handleChange: PropTypes.func,
  value: PropTypes.any,
  items: PropTypes.array,
};
