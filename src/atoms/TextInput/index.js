/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import PropTypes from "prop-types";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import * as defaultTheme from "../default-theme";

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiInputAdornment-positionStart": {
      marginBottom: "4px",
    },
    "& .MuiInputAdornment-positionEnd ": {
      marginBottom: "4px",
    },
    "& .MuiInputBase-multiline": {
      padding: "0",
    },
    "& .MuiInputBase-input": {
      color: `${defaultTheme.colorCodes.darkGray}`,
      fontWeight: "bold",
      fontSize: `${defaultTheme.unit.spacing * 3.5}px`,
      padding: "6px 0 10px 0",
    },
    "& label.Mui-focused": {
      color: `${defaultTheme.colorCodes.darkenGray}`,
    },
    "& .MuiInputLabel-shrink": {
      color: `${defaultTheme.colorCodes.darkenGray}`,
      fontSize: `${defaultTheme.unit.spacing * 4}px !important`,
    },
    "& .MuiInput-underline.Mui-disabled:before ": {
      borderBottomStyle: "solid",
    },
    "& .MuiInput-underline:hover:not(.Mui-disabled):before ": {
      borderBottom: `1px solid  ${defaultTheme.colorCodes.lightningYellow}`,
    },
    "& .MuiInput-underline:before ": {
      borderBottom: `1px solid ${defaultTheme.colorCodes.gray}`,
    },
    "& .MuiInput-underline:after": {
      borderBottom: `1px solid ${defaultTheme.colorCodes.lightningYellow}`,
      "&:hover": {
        borderBottom: `1px solid ${defaultTheme.colorCodes.lightningYellow}`,
      },
    },
    "& .MuiFormHelperText-root": {
      color: `${defaultTheme.colorCodes.error}`,
      fontSize: "12px",
      lineHeight: "14px",
      fontFamily: "Helvetica",
      paddingTop: "6px",
    },
  },
  inputLabel: {
    fontSize: `${defaultTheme.unit.spacing * 3.5}px`,
    lineHeight: "17px",
    letterSpacing: "-0.16px",
    color: `${defaultTheme.colorCodes.darkGray}`,
  },
  inputLabelDisabled: {
    fontSize: `${defaultTheme.unit.spacing * 3.5}px`,
    lineHeight: "17px",
    letterSpacing: "-0.16px",
    color: `${defaultTheme.colorCodes.darkGray}`,
  },
  inputField: {
    width: "100%",
    paddingBottom: `${defaultTheme.unit.spacing * 5}px`,
  },
  disabledComponent: {
    opacity: 0.6,
  },
}));

export default function TextInput(props) {
  const classes = useStyles();
  const [validationError, setError] = useState("");

  const {
    id,
    label,
    type,
    onChange,
    name,
    endAdornment,
    startAdornment,
    readOnly,
    handleClick,
    value,
    disabled,
    maxlength,
    helperText,
    multiline,
    placeholder,
    className,
    style,
    pattern,
    panType,
    ...rest
  } = props;
  return (
    <div
      className={`${disabled ? classes.disabledComponent : ""} ${classes.root}`}
      style={style}
    >
      <TextField
        color="primary"
        placeholder={placeholder}
        helperText={helperText}
        id={id}
        className={classes.inputField}
        InputLabelProps={{
          classes: {
            root: disabled ? classes.inputLabelDisabled : classes.inputLabel,
            focused: "focused",
            shrink: "shrink",
          },
          disabled,
        }}
        label={label}
        type={type}
        onChange={onChange}
        name={name}
        inputProps={{
          maxLength: maxlength,
          className: className && className.formInput,
        }}
        InputProps={{
          readOnly: readOnly ? true : false,
          disabled: disabled ? true : false,
          startAdornment: startAdornment ? (
            <InputAdornment position="start"> {startAdornment} </InputAdornment>
          ) : (
            ""
          ),
          endAdornment: endAdornment ? (
            <InputAdornment position="end">{endAdornment}</InputAdornment>
          ) : (
            ""
          ),
        }}
        onClick={handleClick}
        value={value}
        multiline={multiline}
        {...rest}
      />
    </div>
  );
}

TextInput.propTypes = {
  label: PropTypes.string,
  type: PropTypes.string,
};
