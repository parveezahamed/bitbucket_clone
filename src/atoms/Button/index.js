import React from "react";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import * as defaultTheme from "../default-theme";

const useStyles = makeStyles(() => ({
  root: {
    padding: `0 ${defaultTheme.unit.spacing * 3}px`,
    height: `36px`,
    minWidth: `auto`,
    fontSize: `${defaultTheme.fontSizes.h2.fontSize}`,
    textTransform: "none",
    "&.MuiButton-root": {
      textTransform: "none",
      width: "fit-content",
    },
  },
  primaryButton: {
    border: `1px solid ${defaultTheme.colorCodes.lightningYellow}`,
    backgroundColor: `${defaultTheme.colorCodes.lightningYellow}`,
    color: `${defaultTheme.colorCodes.buttonColor}`,
    "&:hover": {
      backgroundColor: `${defaultTheme.colorCodes.lightningYellow}`,
    },
    "&.MuiButton-root.Mui-disabled": {
      border: `1px solid ${defaultTheme.colorCodes.lightningYellow}`,
      backgroundColor: `${defaultTheme.colorCodes.lightningYellow}`,
      color: `${defaultTheme.colorCodes.buttonColor}`,
    },
  },
  secondaryButton: {
    border: `1px solid ${defaultTheme.colorCodes.lightningYellow}`,
    backgroundColor: `${defaultTheme.colorCodes.lightningYellow}`,
    color: `${defaultTheme.colorCodes.buttonColor}`,
    "&:hover": {
      backgroundColor: `${defaultTheme.colorCodes.lightningYellow}`,
      color: `${defaultTheme.colorCodes.buttonColor}`,
    },
    "&.MuiButton-root.Mui-disabled": {
      border: `1px solid ${defaultTheme.colorCodes.lightningYellow}`,
      color: `${defaultTheme.colorCodes.buttonColor}`,
    },
  },
  buttonLoader: {
    width: "18px",
    height: "18px",
    border: "solid 1px #9d1d28",
    borderRadius: "80px",
    borderBottom: "0",
    animation: `$spin 1s linear infinite`,
    "&:hover": {
      border: "solid 1px #ffffff",
    },
  },
  "@keyframes spin": {
    "100%": {
      "-webkit-transform": "rotate(360deg)",
      transform: "rotate(360deg)",
    },
  },
}));

export default function ButtonField(props) {
  const classes = useStyles();
  const { name, handleClick, type, secondary, disabled, href } = props;
  let isDisabled = disabled ? true : false;
  let buttonClass = `${classes.root} ${
    secondary ? classes.secondaryButton : classes.primaryButton
  } `;
  return (
    <Button
      className={buttonClass}
      onClick={handleClick}
      disabled={isDisabled}
      type={type}
      href={href}
    >
      {name === "ButtonLoader" ? (
        <div className={classes.buttonLoader}></div>
      ) : (
        name
      )}
    </Button>
  );
}

ButtonField.propTypes = {
  name: PropTypes.string.isRequired,
  handleClick: PropTypes.func,
  disabled: PropTypes.bool,
  secondary: PropTypes.bool,
  primary: PropTypes.bool,
  type: PropTypes.string,
};
