import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "12em !important",
    "& .MuiOutlinedInput-root": {
      padding: "0.1em",
      paddingLeft: "0.5em",
      paddingRight: "55px",

      "&:hover": {
        backgroundColor: "#fafbfc",
        cursor: "pointer",
      },
    },
    "& .MuiOutlinedInput-notchedOutline": {
      border: "none",
    },
    "& input": {
      "&::placeholder": {
        textOverflow: "ellipsis !important",
        color: "#172B4D",
        opacity: 1,
        fontSize: "14px",
      },
    },
  },
}));

const SelectAutocomplete = ({ items, placeholder }) => {
  const classes = useStyles();
  return (
    <Autocomplete
      className={classes.root}
      id="combo-box-demo"
      options={items}
      getOptionLabel={(option) => option.title}
      style={{ width: 300 }}
      renderInput={(params) => (
        <TextField
          {...params}
          placeholder={placeholder}
          variant="outlined"
          InputLabelProps={{
            shrink: false,
          }}
        />
      )}
    />
  );
};

SelectAutocomplete.defaultProps = {
  items: [],
};
SelectAutocomplete.propTypes = {};

export default SelectAutocomplete;
