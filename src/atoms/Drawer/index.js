import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";

const useStyles = makeStyles({
  root: {
    "& .MuiDrawer-paper": {
      minWidth: "300px",
      padding: "1em",
    },
  },
});

export default function TemporaryDrawer({
  children,
  anchor,
  isOpen,
  toggleDrawer,
  paperWidth,
}) {
  const classes = useStyles();

  return (
    <div>
      <Drawer
        anchor={anchor}
        open={isOpen}
        onClose={() => toggleDrawer(!isOpen)}
        className={classes.root}
        PaperProps={{
          style: {
            width: paperWidth,
          },
        }}
      >
        {children}
      </Drawer>
    </div>
  );
}
