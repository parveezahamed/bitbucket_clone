import React, { Suspense } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import "./scss/styles.scss";
import Home from "./containers/Home";
import YourWork from "./containers/YourWork";
import Repositories from "./containers/Repositories";
import Projects from "./containers/Projects";
import PullRequests from "./containers/PullRequests";
import Snippets from "./containers/Snippets";
import "react-calendar/dist/Calendar.css";

const App = () => {
  const loading = () => (
    <div className="animated fadeIn pt-1 text-center">Loading...</div>
  );

  return (
    <div>
      <Suspense fallback={loading()}>
        <Router>
          <Routes>
            <Route path="/" element={<Home />}>
              <Route path="/" element={<YourWork />} />
              <Route path="/repositories" element={<Repositories />} />
              <Route path="/projects" element={<Projects />} />
              <Route path="/pull-requests" element={<PullRequests />} />
              <Route path="/snippets" element={<Snippets />} />
            </Route>
          </Routes>
        </Router>
      </Suspense>
    </div>
  );
};

export default App;
