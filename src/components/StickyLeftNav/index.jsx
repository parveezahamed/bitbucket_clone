import React, { useState } from "react";
import { Grid } from "@material-ui/core/";
import { makeStyles } from "@material-ui/core/styles";
import logo from "../../assets/images/logo.svg";
import navigations from "../../assets/images/navigation.svg";
import search from "../../assets/images/search.svg";
import plusIcon from "../../assets/images/plus.svg";
import helpIcon from "../../assets/images/help.svg";
import Drawer from "../../atoms/Drawer";
import TextInput from "../../atoms/TextInput";

const useStyles = makeStyles(() => ({
  container: {
    display: "flex",
    flexDirection: "column",
    flexBasis: "auto",
    flexWrap: "wrap",
    height: "96vh",
  },
  topContainer: {
    flex: 1,
    textAlign: "center",
  },
  bottomContainer: {
    flex: 0,
    textAlign: "center",
  },
  img: {
    padding: "0.4em 0 0.4em 0",
    cursor: "pointer",
  },
}));

const StickyLeftNav = () => {
  const classes = useStyles();

  const [state, setState] = useState(false);
  const [drawerChild, setDrawerChild] = useState("");
  const [paperwidth, setPaperWidth] = useState("200px");

  const toggleDrawer = (event, childName, paperwidth) => {
    setState(event);
    setDrawerChild(childName);
    setPaperWidth(paperwidth);
  };

  const renderDrawerChild = () => {
    switch (drawerChild) {
      case "Search":
        return searchSearchDrawer();
      case "AddRepos":
        return <>Add Repos</>;
      case "SwtichAccount":
        return <>SwitchAccount</>;
      default:
        return <></>;
    }
  };

  const searchSearchDrawer = () => {
    return (
      <>
        <Grid container>
          <Grid item xs={12}>
            <TextInput placeholder="Search for repositories, code and more..." />
          </Grid>
        </Grid>
      </>
    );
  };

  return (
    <>
      <Drawer
        anchor="left"
        isOpen={state}
        toggleDrawer={toggleDrawer}
        paperWidth={paperwidth}
      >
        {renderDrawerChild()}
      </Drawer>
      <div className="sticky_nav" style={{ color: "#ffffff" }}>
        <div className={classes.container}>
          <div className={classes.topContainer}>
            <img className={classes.img} src={logo} alt="logo" />
            <img
              onClick={() => toggleDrawer(true, "Search", "500px")}
              className={classes.img}
              src={search}
              alt="logo"
            />
            <img
              onClick={() => toggleDrawer(true, "AddRepos", "300px")}
              className={classes.img}
              src={plusIcon}
              alt="logo"
            />
          </div>
          <div className={classes.bottomContainer}>
            <img
              onClick={() => toggleDrawer(true, "SwtichAccount", "300px")}
              className={classes.img}
              src={navigations}
              alt="logo"
            />
            <img className={classes.img} src={helpIcon} alt="logo" />
          </div>
        </div>
      </div>
    </>
  );
};

export default StickyLeftNav;
