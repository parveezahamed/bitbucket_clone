import React, { useEffect } from "react";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import queryString from "query-string";
import bitbucketIcon from "../../assets/images/bitbucket.svg";
import yourwork from "../../assets/images/yourwork.svg";
import snip from "../../assets/images/snip.svg";
import repos from "../../assets/images/repos.svg";
import pullrequests from "../../assets/images/pullrequests.svg";
import projects from "../../assets/images/projects.svg";

const useStyles = makeStyles(() => ({
  img: {
    padding: "0.4em 0 0.4em 0",
    width: "7.5em",
  },
  menu: {
    padding: "0",
    marginTop: "1em",
  },
  menuItem: {
    listStyleType: "none",
    padding: "0.5em",
    cursor: "pointer",
    fontWeight: "500",
    paddingTop: "0.8em",
    paddingBottom: "0.8em",

    "&:hover": {
      backgroundColor: "#1C63CE",
      borderRadius: "4px",
    },
  },
  menuImage: {
    width: "1.8em",
    marginRight: "0.8em",
  },
  anchor: {
    color: "#ffffff",

    "&:hover": {
      textDecoration: "none",
      color: "#ffffff",
    },
  },
  activeItem: {
    backgroundColor: "#05367F",
    borderRadius: "4px",
  },
}));

const navItems = [
  {
    name: "Your work",
    icon: "yourWork",
    url: "/",
  },
  {
    name: "Repositories",
    icon: "repositories",
    url: "/repositories",
  },
  {
    name: "Projects",
    icon: "projects",
    url: "/projects",
  },
  {
    name: "Pull requests",
    icon: "pullRequests",
    url: "/pull-requests",
  },
  {
    name: "Snippets",
    icon: "snippets",
    url: "/snippets",
  },
];

const LeftNav = () => {
  const classes = useStyles();

  const renderImages = (icon) => {
    switch (icon) {
      case "yourWork":
        return <img className={classes.menuImage} src={yourwork} alt="logo" />;
      case "repositories":
        return <img className={classes.menuImage} src={repos} alt="logo" />;
      case "projects":
        return <img className={classes.menuImage} src={projects} alt="logo" />;
      case "pullRequests":
        return (
          <img className={classes.menuImage} src={pullrequests} alt="logo" />
        );
      case "snippets":
        return <img className={classes.menuImage} src={snip} alt="logo" />;
      default:
        return null;
    }
  };

  const navigation = (naviaget) => {
    window.location.href = naviaget;
  };

  const isActiveRoute = (url) =>
    url === window.location.pathname ? true : false;

  const renderNavs = () => {
    return (
      <ul className={classes.menu}>
        {navItems.map((item, index) => {
          return (
            <li
              key={index}
              onClick={() => navigation(item.url)}
              className={classNames(classes.menuItem, {
                [classes.activeItem]: isActiveRoute(item.url),
              })}
            >
              <a className={classes.anchor} href={item.url}>
                {renderImages(item.icon)} {item.name}
              </a>
            </li>
          );
        })}
      </ul>
    );
  };

  return (
    <div className="expandable_nav">
      <img className={classes.img} src={bitbucketIcon} alt="logo" />
      {renderNavs()}
    </div>
  );
};

export default LeftNav;
