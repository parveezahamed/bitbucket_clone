import React from "react";
import classNames from "classnames";
import moment from "moment";
import { makeStyles } from "@material-ui/core/styles";
import defaultImage from "../../assets/images/default.png";
import phpImage from "../../assets/images/php.png";
import jsImage from "../../assets/images/js.png";
import reactImage from "../../assets/images/default.png";
import nodeImage from "../../assets/images/nodejs.png";
import lockImage from "../../assets/images/lock.svg";

const useStyles = makeStyles(() => ({
  tableContainer: {
    width: "calc(100% - 5%)",
    margin: "0 auto",
  },
  ul: {
    padding: 0,
    margin: 0,
  },
  li: {
    listStyleType: "none",
    verticalAlign: "middle",
    "&:hover": {
      backgroundColor: "#F4F5F7",
    },
  },
  inline: {
    display: "inline-block",
    margin: "auto",
    marginRight: "10px",
    verticalAlign: "middle",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
    maxWidth: "100%",
    overflow: "hidden",
  },
  customWidht: {
    minWidth: "calc(100% - 80%)",
    padding: "0 20px 0 20px",
  },
  lastItem: {
    textAlign: "right",
    width: "25%",
  },
  block: {
    display: "block",
  },
  mainBlockBorder: {
    padding: "12px 0 12px 0",
  },
  langImg: {
    width: "35px",
    borderRadius: "5px",
  },
  repoName: {
    fontWeight: 500,
    color: "#333",

    "&:hover": {
      color: "inherit",
    },
  },
  smallFonts: {
    fontSize: "12px",
    color: "#6B778C",

    "&:hover": {
      color: "#6B778C",
    },
  },
  headerContainer: {
    borderBottom: "2px solid #DFE1E6",
    padding: "10px 0 10px 0",
    color: "#5E6C84",
    fontWeight: 500,
    width: "100%",
    fontSize: "16px",
  },
  headerList: {
    listStyleType: "none",
    display: "inline-block",
    marginRight: "20px",
  },
  summary: {
    width: "calc(100% - 70%)",
  },
  description: {
    width: "calc(100% - 70%)",
  },
  builds: {
    width: "calc(100% - 80%)",
  },
  update: {
    width: "calc(100% - 88%)",
    textAlign: "right",
  },
  noData: {
    margin: "1em",
    textAlign: "center",
  },
}));

const RepoTable = ({ data, isData }) => {
  const classes = useStyles();

  const renderImages = (lang) => {
    switch (lang) {
      case "React Js":
        return <img src={reactImage} alt={lang} className={classes.langImg} />;
      case "Javascript":
        return <img src={jsImage} alt={lang} className={classes.langImg} />;
      case "PHP":
        return <img src={phpImage} alt={lang} className={classes.langImg} />;
      case "Node Js":
        return <img src={nodeImage} alt={lang} className={classes.langImg} />;
      default:
        return (
          <img src={defaultImage} alt={lang} className={classes.langImg} />
        );
    }
  };

  const renderTable = () => {
    return data.map((item, index) => {
      return (
        <li
          key={index}
          className={classNames(classes.li, classes.mainBlockBorder)}
        >
          <ul className={classes.ul}>
            <li className={classNames(classes.li, classes.inline)}>
              <div>{renderImages(item.lang)}</div>
            </li>
            <li className={classNames(classes.li, classes.inline)}>
              <div className={classNames(classes.block, classes.repoName)}>
                <a href="/" className={classes.repoName}>
                  {item.repoName}
                </a>
              </div>
              <div className={classNames(classes.block, classes.smallFonts)}>
                <a href="/" className={classes.smallFonts}>
                  {item.ownerName}
                </a>{" "}
                /{" "}
                <a href="/" className={classes.smallFonts}>
                  {item.title}
                </a>{" "}
                -{" "}
                <a href="/" className={classes.smallFonts}>
                  {moment(item.updatedDate).format("YYYY-MM-DD")}
                </a>
              </div>
            </li>
            <li
              className={classNames(
                classes.li,
                classes.inline,
                classes.customWidht
              )}
            >
              {item.description}
            </li>
            <li
              className={classNames(
                classes.li,
                classes.inline,
                classes.customWidht
              )}
            >
              {item.buildNumber}
            </li>
            <li
              className={classNames(
                classes.li,
                classes.inline,
                classes.customWidht,
                classes.lastItem
              )}
            >
              {item.isPrivate ? <img src={lockImage} alt="Private" /> : ""}
            </li>
          </ul>
        </li>
      );
    });
  };

  const noData = () => {
    return (
      <>
        <div className={classes.noData}>
          <h4>No repositories match</h4>
          <p>
            Try modifying your filter criteria or{" "}
            <a href="/">Create your own repository</a>.
          </p>
        </div>
      </>
    );
  };

  return (
    <div className={classes.tableContainer}>
      <div className={classes.headerContainer}>
        <ul className={classes.ul}>
          <li className={classNames(classes.headerList, classes.summary)}>
            Summary
          </li>
          <li className={classNames(classes.headerList, classes.description)}>
            Description
          </li>
          <li className={classNames(classes.headerList, classes.builds)}>
            Builds
          </li>
          <li className={classNames(classes.headerList, classes.update)}>
            Last updated
          </li>
        </ul>
      </div>
      {isData ? <ul className={classes.ul}>{renderTable()}</ul> : noData()}
    </div>
  );
};

export default RepoTable;
