import React, { useState } from "react";
import moment from "moment";
import { Grid } from "@material-ui/core/";
import { makeStyles } from "@material-ui/core/styles";
import Calendar from "react-calendar";
import inputSearchIcon from "../../assets/images/Inputsearch.svg";
import SelectAutoComplete from "../../atoms/Autocomplete";

const useStyles = makeStyles(() => ({
  headerContainer: {
    padding: "0 25px 25px 25px",
  },
  inputSearchContainer: {
    position: "relative",
  },
  inputSearch: {
    padding: "7px 35px 7px 8px",
    border: "2px solid #DFE1E6",
    width: "100%",
    backgroundColor: "#fafbfc",
    backgroundImage: `url(${inputSearchIcon})`,
    backgroundRepeat: "no-repeat",
    backgroundPosition: "95%",
    backgroundSize: "11%",
  },
}));

const TableFilter = ({ data, searchedData }) => {
  const classes = useStyles();

  const [search, setSearch] = useState("");
  const [filterdDate, setFilteredDate] = useState(new Date());

  const handleSearch = (event) => {
    setSearch(event.target.value);

    const filtered = handleFilter(handleFilter);
    searchedData(filtered);
  };

  const handleFilter = (values) => {
    return data.filter((entry) =>
      Object.values(entry).some(
        (val) => typeof val === "string" && val.includes(values)
      )
    );
  };

  const onChange = (date) => {
    const filtered = handleFilter(moment(date).format("YYYY-MM-DD"));
    searchedData(filtered);
  };

  return (
    <div className={classes.headerContainer}>
      <Grid container spacing={1}>
        <Grid item xs={2}>
          <div className={classes.inputSearchContainer}>
            <input
              type="text"
              placeholder="Search repositories"
              className={classes.inputSearch}
              value={search}
              onChange={(event) => handleSearch(event)}
            />
          </div>
        </Grid>
        <Grid item xs={2}>
          <SelectAutoComplete placeholder="Workspace" />
        </Grid>
        <Grid item xs={2}>
          <SelectAutoComplete placeholder="Project" />
        </Grid>
        <Grid item xs={2}>
          <SelectAutoComplete placeholder="Type" />
        </Grid>
        <Grid item xs={2}>
          <Calendar onChange={onChange} value={filterdDate} />
        </Grid>
      </Grid>
    </div>
  );
};

export default TableFilter;
